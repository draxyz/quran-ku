importScripts('https://unpkg.com/workbox-sw@4.3.1/build/workbox-sw.js');

workbox.setConfig({debug:true});
workbox.precaching.precacheAndRoute([]);

// workbox.routing.registerNavigationRoute("__PUBLIC/index.html");

workbox.routing.registerRoute(
  // Cache image files.
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  // Use the cache if it's available.
  new workbox.strategies.CacheFirst({
    // Use a custom cache name.
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Cache only 50 images.
        maxEntries: 50,
        // Cache for a maximum of a week.
        maxAgeSeconds: 7 * 24 * 60 * 60,
      })
    ],
  })
);

workbox.routing.registerRoute(
  // Cache cdn files.
  /.*(?:googleapis|gstatic|unpkg)\.com/,
  // Use the cache if it's available.
  new workbox.strategies.CacheFirst({
    // Use a custom cache name.
    cacheName: 'static-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Cache only 20 static.
        maxEntries: 20,
        // Cache for a maximum of a year.
        maxAgeSeconds: 365 * 24 * 60 * 60,
      })
    ],
  })
);




// const v = "0";
// addEventListener('install', e => e.waitUntil(
  // caches.open(v).then(cache => cache.addAll(['/']))
// ));

// addEventListener('fetch', e => {
  // console.log('fetch', e.request);
  // e.respondWith(
    // caches.match(e.request).then(cachedResponse =>
      // cachedResponse || fetch(e.request)
    // )
  // );
// });

// addEventListener('activate', e => {
  // e.waitUntil(caches.keys().then(keys => {
    // return Promise.all(keys.map(key => {
      // if (key != v) return caches.delete(key);
    // }));
  // }));
// });

addEventListener('message', e => {
  if (e.data === 'skipWaiting') {
    skipWaiting();
  }
});
// workbox.routing.registerRoute(
  // new RegExp('/.*'),
  // new workbox.strategies.NetworkFirst()
// )

// addEventListener('message', e => {
  // if (e.data === 'skipWaiting') {
    // skipWaiting();
  // }
// });