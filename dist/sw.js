importScripts('https://unpkg.com/workbox-sw@4.3.1/build/workbox-sw.js');

workbox.setConfig({debug:true});
workbox.precaching.precacheAndRoute([
  {
    "url": "app.26eda1ee.js",
    "revision": "58cffd471aaf6d83d96f9a247d287d1e"
  },
  {
    "url": "background.1ebd5533.svg",
    "revision": "87e1e11d8682875ec6389af158838756"
  },
  {
    "url": "drax.741f7e11.css",
    "revision": "454a2fe3d2e3d45c9c6792a9e9f49600"
  },
  {
    "url": "entypo.01cc595a.9839cbe5.woff",
    "revision": "21bb3d10f52406b454823967609ae327"
  },
  {
    "url": "entypo.38a7b678.d85c649f.css",
    "revision": "784f4aa0f58894fce2abdca94f4bf927"
  },
  {
    "url": "entypo.4b16f478.bf369823.svg",
    "revision": "224ad475ed8ee427c6769ef4cbdbbdcc"
  },
  {
    "url": "entypo.dfe62249.6ba1d940.ttf",
    "revision": "5809aa00eec2e89ab4f93b55504a2620"
  },
  {
    "url": "entypo.f9fc0f86.a69aa153.eot",
    "revision": "5c5e5e786d6da140e51a72095a0885a1"
  },
  {
    "url": "favicon.e07ff2b6.ico",
    "revision": "e558852ac4f23e15dc3f4647a34609a3"
  },
  {
    "url": "footer-bg.dee8f36b.jpg",
    "revision": "cb9eb934fef8ffadb3fe54714fa3b37e"
  },
  {
    "url": "icon-144x144.3660a9f5.png",
    "revision": "f38d108c2253a39432080bb3cbd09f38"
  },
  {
    "url": "icon-192x192.b7b273dc.png",
    "revision": "03cc4ad64e7103c47b7e60e2d04d8a32"
  },
  {
    "url": "icon-48x48.c285d8ea.png",
    "revision": "32b26e5a18fcaed7ed93dce1f61460ba"
  },
  {
    "url": "icon-512x512.e032531d.png",
    "revision": "7483a8342c7c24a7004b622c0f4bdd95"
  },
  {
    "url": "icon-72x72.b38b9423.png",
    "revision": "b458b46c4a2bb11c27c3470925b52229"
  },
  {
    "url": "icon-96x96.29fbccdf.png",
    "revision": "72d82b0369eacd98179c9b6db2223dde"
  },
  {
    "url": "index.html",
    "revision": "e796a72f03014edabfa4a8b38d2ac2f4"
  },
  {
    "url": "manifest.webmanifest",
    "revision": "4d44482fdcd01cf4f582ad3d13870eed"
  },
  {
    "url": "utsmani.44b47739.css",
    "revision": "bdc492022b930c2497c7b80fae68c428"
  },
  {
    "url": "utsmani.eafe54d3.otf",
    "revision": "43269f118299246de0cf264e04ae2680"
  },
  {
    "url": "/",
    "revision": "7577a107f738512b8c44c58d0ccfb4ba"
  }
]);

// workbox.routing.registerNavigationRoute(".//index.html");

workbox.routing.registerRoute(
  // Cache image files.
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  // Use the cache if it's available.
  new workbox.strategies.CacheFirst({
    // Use a custom cache name.
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Cache only 50 images.
        maxEntries: 50,
        // Cache for a maximum of a week.
        maxAgeSeconds: 7 * 24 * 60 * 60,
      })
    ],
  })
);

workbox.routing.registerRoute(
  // Cache cdn files.
  /.*(?:googleapis|gstatic|unpkg)\.com/,
  // Use the cache if it's available.
  new workbox.strategies.CacheFirst({
    // Use a custom cache name.
    cacheName: 'static-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Cache only 20 static.
        maxEntries: 20,
        // Cache for a maximum of a year.
        maxAgeSeconds: 365 * 24 * 60 * 60,
      })
    ],
  })
);




// const v = "0";
// addEventListener('install', e => e.waitUntil(
  // caches.open(v).then(cache => cache.addAll(['/']))
// ));

// addEventListener('fetch', e => {
  // console.log('fetch', e.request);
  // e.respondWith(
    // caches.match(e.request).then(cachedResponse =>
      // cachedResponse || fetch(e.request)
    // )
  // );
// });

// addEventListener('activate', e => {
  // e.waitUntil(caches.keys().then(keys => {
    // return Promise.all(keys.map(key => {
      // if (key != v) return caches.delete(key);
    // }));
  // }));
// });

addEventListener('message', e => {
  if (e.data === 'skipWaiting') {
    skipWaiting();
  }
});
// workbox.routing.registerRoute(
  // new RegExp('/.*'),
  // new workbox.strategies.NetworkFirst()
// )

// addEventListener('message', e => {
  // if (e.data === 'skipWaiting') {
    // skipWaiting();
  // }
// });