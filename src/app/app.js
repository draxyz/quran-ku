import { h, app } from "hyperapp";
import { set, Location, db, timeout, request } from "./helpers/";
import { view } from "./views/";
// import { initAuth } from "./views/auth/";
import { initQuran } from "./views/quran/";
import './plugins/register-sw.js';

{/* Config DB Storage */}
const stores={
	// auth:initAuth,
	quran:initQuran
}

{/* Set Initial State */}
const init = state=>[
  state,
  set('app',{
		url:"https://tadarusanyuk.jatimprov.go.id/api", //api url
    page:location.hash||'#!/home',
    current:location.hash.split('/').pop(),
    loading:true,
		acara:[
			{id:'',judul:'',deskripsi:'',linkyoutube:''},
			{id:'',judul:'',deskripsi:'',linkyoutube:''},
			{id:'',judul:'',deskripsi:'',linkyoutube:''},
			{id:'',judul:'',deskripsi:'',linkyoutube:''},
		],
		jadwalpuasa:[]
  }),
	// loadStores()
	timeout(loadJadwal,500),
	timeout(loadAcara,500),
	timeout(getDB,0),
]


const loadJadwal=state=>[
	state,	
	request.get({
		url:`${state.app.url}/jadwalpuasa/get?pin=ToQrgzx018`,
		action:(state,response)=>[
			state,
			console.log(response),
			set('app.jadwalpuasa',response),
			// setHash('#!/quran')
		],
		error:(state,response)=>[
			state,
			console.log(response)
		]
	})
]

const loadAcara=state=>[
	state,	
	request.get({
		url:`${state.app.url}/acara/get?pin=ToQrgzx018`,
		action:(state,response)=>[
			state,
			console.log(response),
			set('app.acara',response),
			// setHash('#!/quran')
		],
		error:(state,response)=>[
			state,
			console.log(response)
		]
	})
]
const loadStores=state=>[
	state,
	Object.keys(stores).map(key=>
		set(key,stores[key])
	),
	set('app.loading',false,500),
]
{/* Get state from DB */}
const getDB=state=>[
	state,
	console.log('Get state',state),
	db.getItems({
		stores:stores,
		action:(state,results)=>[
			state,
			set('app.loading',false),
			Object.keys(results).map(key=>
				set(key,results[key])
			)
		]
	})
]
{/* Save state to DB */}
const setDB=state=>[
	state,
	console.log('Saving state',state),
	Object.keys(stores).map(key=>
		db.setItem(key,state[key])
	)
]

{/* Main Application */}
app({
	init: init,
	view: view,
	node: document.getElementById("app"),
	subscriptions:state=>[
    Location.changed(),
		timeout(setDB,0)
	]
})


