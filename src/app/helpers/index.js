import { h } from "hyperapp";
import localforage from "localforage";
import { localforageGetItems } from "localforage-getitems";


export const fx=f=>props=>[f,props]
export const get=(store,data)=>{
  return store.split('.').reduce((p,c)=>p&&p[c]||null,data)
}
export const merge=(...props)=>{
	return Object.assign(...props)
}
export const set=(store,data,delay)=>fx((dispatch,props)=>{
  if(props.delay){
    setTimeout(()=>dispatch(_set,props),props.delay)
  }else{
    dispatch(_set,props)
  }
})(typeof store==='object'? store : {store,data,delay})

export const run=(action,delay)=>fx((dispatch,props)=>{
  if(props.delay){
    setTimeout(()=>dispatch(props.action,props),props.delay)
  }else{
    dispatch(props.action,props)
  }
})(typeof action!=='function'? action : {action,delay})

export const timeout=(action,delay)=>fx((dispatch,props)=>{
  var id=setTimeout(()=>dispatch(props.action,props),props.delay)
  return ()=>{clearTimeout(id)}
})(typeof action!=='function'? action : {action,delay})

export const interval=(action,delay)=>fx(function(dispatch, props) {
  var id = setInterval(()=>{dispatch(props.action, props)}, props.delay)
  console.log(id)
  return ()=>{clearInterval(id)}
})(typeof action!=='function'? action : {action,delay})


export const assign = (obj, path, val) => { 
  var keys = path.split('.');
  var lastKey = keys.pop();
  var lastObj = keys.reduce((obj, key) => {
		// console.log(path,Array.isArray(obj[key]),obj[key])
		return obj[key] = obj[key] || [];
	}, obj); 
  if(typeof lastObj[lastKey]!=='object'||Array.isArray(val)||!val.length){
    lastObj[lastKey] = val;
  }else{
		lastObj[lastKey] = {...lastObj[lastKey],...val};
  }
  return obj;
};

export const _set=(state,props)=>{
  if(props.store&& typeof props.data!=='undefined'){
		return {...state,...assign(state,props.store,props.data)}
  }else{
    return {...state,...props}
  }
}

export const setHash=(hash)=>[()=>location.hash=hash,null];
const http=(dispatch,props)=>{
	fetch(props.url,props.options)
    .then(response=>{
      if(!response.ok){throw response}
      return response.json()
    }).then(response=>{
      dispatch(props.action, response)
    }).catch(function(err) {
      if(err.text){
        err.text().then(msg=> {
          dispatch(props.error,JSON.parse(msg))
        })
      }else{
        dispatch(props.error,{text:'Network failed'})
      }
    })
}

export const request ={
	get:fx((dispatch, props) => {
		props.options=merge({method:'get'},props.options)
		http(dispatch,props)
	}),
	post:fx((dispatch, props) => {
		props.options=merge({method:'post'},props.options)
		http(dispatch,props)
	}),
	put:fx((dispatch, props) => {
		props.options=merge({method:'put'},props.options)
		http(dispatch,props)
	}),
	delete:fx((dispatch, props) => {
		props.options=merge({method:'delete'},props.options)
		http(dispatch,props)
	})
}

const listen=(el,dispatch, action, name) =>{
  var listener=e=>dispatch(action,e) 
  el.addEventListener(name,listener) 
  return ()=>el.removeEventListener(name,listener)
}


export const Online={
  set:state=>[state,set('app.online',navigator.onLine)],
  changed:fx((dispatch,props)=>{
    listen(window,dispatch,Online.set,'online')
    listen(window,dispatch,Online.set,'offline')
  })
}

export const Location={
  set:(state)=>[state,set('app.page',location.hash)],
  //changed:fx((dispatch,props)=>listen(window,dispatch,Location.set,"hashchange"))
  changed:fx((dispatch,action)=>listen(window,dispatch,Location.set,"hashchange"))
}


localforage.getItems=localforageGetItems


export const db={
	setItem: (store,data)=>fx((dispatch, props) => {
		if(data!==undefined){
			data=JSON.parse(JSON.stringify(data));
			Object.keys(data).map(key=>{
				// console.log(data[key])
				if(data[key].cache!==true){
					data[key]={}
				}
			})
			localforage.setItem(store,data)
		}
	})(typeof store==='object'? store : {store,data}),
	
	getItems : fx((dispatch, props) => {
		localforage.getItems().then(function(results){
			Object.keys(props.stores).map(key=>{
				results[key]=results[key]||props.stores[key]
			})
			dispatch(props.action,results)
		})
	}),
	clear:fx((dispatch,props)=>{
		localforage.clear()
	})
}






export class insertHTML extends HTMLElement {
	static get observedAttributes() {
		return ["html"];
	}
	constructor() {
		super();
	}
	connectedCallback() {
		this.innerHTML=this.getAttribute('html');
	}
	attributeChangedCallback(name, oldValue, newValue) {
		this.innerHTML=this.getAttribute('html');
		// this.innerHTML=this.dataset.html;
	}

}

customElements.define('html-insert',insertHTML);