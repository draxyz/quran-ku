import { h } from "hyperapp"
import { set } from "../../helpers/"
import { auth } from "../auth/"

export const Error=state=>(
	<div class="tile">
		<div class="dropdown">
			<a class="button silver text-black" data-dropdown tabindex="0">Not Found &#x25BE;</a>
			<ul class="menu">
				<li><a href="#!/">Dashboard</a></li>
				<li><a href="#!/tasks">Tasks</a></li>
				<li><a href="#!/file">File</a></li>
				<li><a href="#!/users">Users</a></li>
			</ul>
		</div>
		<div/>
		
		<div class="action dropdown">
			<a class="button link px-1" data-dropdown tabindex="0">
				<i class="icon-menu"/>
			</a>
			<ul class="menu">
				<li><a href="#!/" onclick={auth.logoutSubmit}>Logout</a></li>
			</ul>
		</div>
	</div>
)