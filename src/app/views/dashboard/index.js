import { h } from "hyperapp"
import { set } from "../../helpers/"
import { auth } from "../auth/"


export const Dashboard=state=>(
	<div class="relative">
		<div class="img-banner">
			<img alt="" src="/images/avatar.png" class="img-title"/>
		</div>
		<div class="py-5">
			<h1 class="p-0">
				<small class="text-muted">Hi, </small> 
				{state.auth.logged.username}
			</h1>
			<p class="text-muted m-0">Salary man</p>
		</div>
		<div class="navbar my-3">
			<p class="extra-large">
				<span class=" border-bottom-left">Projects</span>
			</p>
			<a class="text-primary tooltip-left" data-tooltip="See All"><span class="icon-dot-3"></span></a>
		</div>
		
		<div class="overflow-x">
			<div class="w-450 w-sm-250 w-lg-200 w-xl-100">
				<div class="row gap six">
					<div>
						<article class="card primary text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
					<div>
						<article class="card secondary text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
					<div>
						<article class="card info text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
					<div>
						<article class="card success text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
					<div>
						<article class="card warning text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
					<div>
						<article class="card error text-white rounded">
							<h2 class="px-1 opacity-25"><span class="icon-suitcase"></span></h2>
							<p class="py-0">
								<small class="text-silver">25 Tasks</small><br/>
								<b>Lorem</b>
							</p>
							<footer class="stack">
								<img alt="" src="/images/avatar.png" class="avatar"/>
								<img alt="" src="/images/avatar.png" class="avatar"/>
							</footer>
						</article>
					</div>
				</div>
			</div>
		</div>

			
				<div class="navbar my-3">
					<p class="large">
						<a href="#!/dashboard/today" class="text-black border-bottom-left">Today</a>
						<a href="#!/dashboard/week" class="text-muted">Week</a>
						<a href="#!/dashboard/month" class="text-muted">Month</a>
					</p>
					<a class="text-primary tooltip-left" data-tooltip="See All"><span class="icon-dot-3"></span></a>
				</div>
				
				<div class="row one three-md">
					<div>
						<article class="tile mt-0 rounded">
							<img alt="" src="/images/avatar.png" class="avatar small mr-2"/>
							<section class="py-1">
								<h5>Coba</h5>
								<span class="text-gray">Lorem ipsum</span>
							</section>
						</article>
					</div>
					<div>
						<article class="tile mt-0 rounded">
							<img alt="" src="/images/avatar.png" class="avatar small mr-2"/>
							<section class="py-1">
								<h5>Coba</h5>
								<span class="text-gray">Lorem ipsum</span>
							</section>
						</article>
					</div>
					<div>
						<article class="tile mt-0 rounded">
							<img alt="" src="/images/avatar.png" class="avatar small mr-2"/>
							<section class="py-1">
								<h5>Coba</h5>
								<span class="text-gray">Lorem ipsum</span>
							</section>
						</article>
					</div>
				</div>
			



		</div>
)
	