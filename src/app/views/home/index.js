import { h } from "hyperapp"
import { set } from "../../helpers/"
import { actionQuran } from "../quran/"
// import { auth } from "../auth/"


const Infopuasa=state=>(
	<div class="py-2">		
		<div class="px-4 px-md-5">
			<h4 class="border-bottom-left inline-block">Apa yang harus Dilakukan ?</h4>
			<p class="large">Dalam kondisi : 
				<span class="mx-1 border-bottom"><b>Sehat</b></span>
				<span class="mx-1">Sakit</span>
			</p>
		</div>
		<div class="overflow-x px-4 px-md-5">
			<div class="row six three-md w-300 w-md-100">
				<div class="p-1">
					<article class="card rounded">
						<img src="https://placeimg.com/400/240/any1"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				<div class="p-1">
					<article class="card rounded">
						<img src="https://placeimg.com/400/240/any2"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				<div class="p-1">
					<article class="card rounded">
						<img src="https://placeimg.com/400/240/any3"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				<div class="p-1">
					<article class="card">
						<img src="https://placeimg.com/400/240/any4"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				<div class="p-1">
					<article class="card">
						<img src="https://placeimg.com/400/240/any5"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				<div class="p-1">
					<article class="card">
						<img src="https://placeimg.com/400/240/any6"/>
						<p>This is a nice forest. What do you want to do with it?</p>
					</article>
				</div>
				
			</div>
		</div>
		
	</div>
)
const Infotadarus=state=>(
	<div class="py-2">
		
		<div class="px-4 px-md-5">
			<h4 class="border-bottom-left inline-block">Live Streaming Tadarus Online</h4>
		</div>
		<div class="overflow-x px-4 px-md-5 py-2">
			<div class="row four two-md w-300 w-md-100">
				{state.app.acara.map(a=>(					
					<div class="p-1">
						<article class="card rounded">
							<iframe src={a.linkyoutube.replace('watch?v=','embed/')} style={{width:'100%',height:'20em',border:0}}></iframe>
							<section class="p-3">
								<h4>{a.judul}</h4>
								<p>{a.deskripsi}</p>
							</section>
						</article>
					</div>
				))}
			</div>
		</div>
		
	</div>
)

const Footer=state=>(
	<div class="footer px-5 text-white">
	<div class="row one three-md">
		<div class="py-5">
			<nav>
				<h3>Tautan</h3>
				<ul>
					<li>
						<a href="https://jatimprov.go.id" target="_blank" class="text-white"><i class="icon-right-thin mr-1"></i>Website Jatimprov</a>
					</li>
					<li>
						<a href="http://dinkes.jatimprov.go.id" target="_blank" class="text-white"><i class="icon-right-thin mr-1"></i>Dinas Kesehatan Jatim</a></li>
					<li><a href="http://dindik.jatimprov.go.id" target="_blank" class="text-white"><i class="icon-right-thin mr-1"></i>Dinas Pendidikan Jatim</a></li>
					<li><a href="https://kominfo.jatimprov.go.id" target="_blank" class="text-white"><i class="icon-right-thin mr-1"></i>Dinas Kominfo Jatim</a></li>
					<li><a href="http://bpbd.jatimprov.go.id" target="_blank" class="text-white"><i class="icon-right-thin mr-1"></i>BPBD Jatim</a></li>
				</ul>
			</nav>
		</div>
		<div class="py-5">
			<nav>
				<h3>Media Sosial</h3>
				<ul>
					<li><a href="https://twitter.com/KhofifahIP" target="_blank" class="text-white"><i class="icon-twitter mr-1"></i>Twitter Ibu Gubernur Jatim</a></li>
					<li><a href="https://twitter.com/emildardak" target="_blank" class="text-white"><i class="icon-twitter mr-1"></i>Twitter Wakil Gubernur Jatim</a></li>
					<li><a href="https://twitter.com/jatimpemprov" target="_blank" class="text-white"><i class="icon-twitter mr-1"></i>Twitter Pemprov Jatim</a></li>
					<li><a href="https://www.facebook.com/JatimPemprov/" target="_blank" class="text-white"><i class="icon-facebook mr-1"></i>Facebook Pemprov Jatim</a></li>
					<li><a href="https://www.facebook.com/JatimPemprov/" target="_blank" class="text-white"><i class="icon-instagram mr-1"></i>Instagram Pemprov Jatim</a></li>
				</ul>
			</nav>
		</div>
		<div class="py-5">
			<nav>
				<h3>Hubungi</h3>
				<ul>
					<li><a href="tel:1500117" class="text-white"><i class="icon-phone mr-1"></i>Call center 1500 117</a></li>
					<li><a class="text-white"><i class="icon-address mr-1"></i>Posko Informasi Corona di Gedung Negara Grahadi<br/> Jl. Gubernur Suryo, Embong Kaliasin, Kec. Genteng, Kota SBY, Jawa Timur 60271</a></li>
					<br/>
					<li><a href="https://idraxy.web.app" target="_blank" class="text-white"><i class="icon-user mr-1"></i>Designed By Drax</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="credit text-center p-2">Copyright © 2020 <span class="text-primary">Dinas Komunikasi dan Informatika Provinsi Jawa Timur</span></div>
</div>
)

const Headline=state=>(
	<div class="p-4 p-md-5">
		<div class="row one two-md" style={{minHeight:"65vh"}}>
			<div class="row middle">
				<div>
					<h3>Marhaban Ya Ramadhan</h3>
					<p class="large py-2">
						Mari bergabung menyambut ramadhan tahun ini bersama Tadarus Online Jawa Timur
					</p>
					<a onclick={actionQuran.getDaftar} class="button info large">Baca Alquran Sekarang</a>
				</div>
			</div>
			<div class="row middle">
				<img src="/images/headline-bg.jpg"/>
			</div>
		</div>
	</div>
)
		// {Infopuasa(state)}
export const Home=state=>(
	<div class="relative">
		<div class="bg-over-top primary"></div>
		<div class="bg-top-circle" style={{zIndex:'-1'}}></div>
		<header class="navbar p-5">
			<section class='mr-5 text-bold extra-large'>
				<img src="images/logo.png" width='80' style={{float:'left'}}/>
				<a href="#!/home" class="text-white py-2" >Tadarusan Yuk !!</a>
			</section>
			<section>
			</section>
			<section class='ml-5 text-bold'>
				<a href="#!" class="text-white border-bottom-left my-1" >Beranda</a>
				<a href="#!/quran" class="text-white border-bottom-left my-1">Baca Quran</a>
				<a href="#!" class="text-white border-bottom-left my-1">Jadwal</a>
				<a href="#!" class="text-white border-bottom-left my-1">Tadarus Online</a>
			</section>
		</header>
		{Headline(state)}
		{Infotadarus(state)}
		<Footer/>
	</div>
)
	
	