import { h } from "hyperapp"
import { set } from "../../helpers/"
// import { auth } from "../auth/"

const Header=state=>(
	<div>
	<div class="oval-bg red"></div>
	<header class="navbar px-5">
		<section>
			<img src="/images/logo.png" class=""/>
		</section>
		<section class='none ml-5  text-bold show-md large'>
			<a href="#!/home" class="text-white border-bottom-left" >Beranda</a>
			<a href="#!/home" class="text-white">Ruang Dekranasda</a>
			{/*	<a href="#!/home">Aktivitas</a>
			<a href="#!/home">Kolaborasi</a>
			<a href="#!/home">Inspirasi</a>
			<a href="#!/home">Daftar</a>
			*/}
			<a href="#!/home" class="text-white">Berita</a>
			<a href="#!/home" class="text-white">Tentang</a>
			<a href="#!/home" class="text-white"><i class="icon-search"/></a>
		</section>
	</header>
	</div>
)
const Headline=state=>(
	<div class="carousel">
		<input id="slide-1" type="radio" name="carousel" checked/>
		<input id="slide-2" type="radio" name="carousel"/>
		<input id="slide-3" type="radio" name="carousel"/>
		<section>
			<div>
				<img src="http://placeimg.com/640/360/any"/>
			</div>
			<div>
				<img src="http://placeimg.com/640/360/any"/>
			</div>
			<div>
				<img src="http://placeimg.com/640/360/any"/>
			</div>
			<div>
				<img src="http://placeimg.com/640/360/any"/>
			</div>
		</section>
		<div class="indicators">
			<label for="slide-1">1</label>
			<label for="slide-2">2</label>
			<label for="slide-3">3</label>
			<label for="slide-4">4</label>
		</div>
	</div>
)
export const Home=state=>(
	<div class="relative">
		
		<Header/>
		<div>
			<div class="row five ml-3">
				<div class="row one middle xs-2">
					<div class="ml-5">
						<div class="h1">Headline title</div>
						<p class="large">
							Lorem ipsum blablbalbalba
							<br/>
							<br/>
							<a href="">Telusuri <i class="icon-right-thin"/></a>
						</p>
					</div>
				</div>
				<div class="row six fill">
					<div class="xs-5">
						<img src="http://placeimg.com/360/480/any" class="w-100 p-0"/>
					</div>
					<div class='row one bottom'>
						<a href="" class="button extra-large error radius-0 p-0 mx-0"><i class="icon-right-open-big"/></a>
					</div>
				</div>
			</div>
			
			<nav style={{margin:"-1em 2.5em 3em"}}>
				<a href="" class="mr-2"><i class="icon-facebook"/> FACEBOOK</a>
				<a href="" class="mr-2"><i class="icon-twitter"/> TWITTER</a>
				<a href="" class="mr-2"><i class="icon-instagram"/> INSTAGRAM</a>
			</nav>
		</div>
		
		<div class="mx-5 p-5 border-bottom">		
			<div class="row gap two three-sm">
				<div>
					<img src="http://placeimg.com/320/450/any" class="w-100"/>
				</div>
				<div>
					<img src="http://placeimg.com/320/450/anya" class="w-100"/>
				</div>
				<div class="row middle fill">
					<div class="p-3">
						<h2>Ruang Dekranasda</h2>
						<p>
							Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
						</p>
					</div>
				</div>
			</div>			
		</div>
		
		<div class="p-5" style={{backgroundImage:"url(/images/background.svg)"}}>		
			<div class="row gap two four-md text-center">
				<div>
					<div class="display-1 p-0 text-error"><i class="icon-palette"/></div>
					<h3>Aktivitas</h3>
					<p>
						Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
					</p>
				</div>
				<div>
					<div class="display-1 p-0 text-error"><i class="icon-feather"/></div>
					<h3>Inspirasi</h3>
					<p>
						Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
					</p>
				</div>
				<div>
					<div class="display-1 p-0 text-error"><i class="icon-picture"/></div>
					<h3>Galeri</h3>
					<p>
						Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
					</p>
				</div>
				<div>
					<div class="display-1 p-0 text-error"><i class="icon-google-circles"/></div>
					<h3>Kolaborasi</h3>
					<p>
						Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
					</p>
				</div>
			</div>			
		</div>
		
		<div class="p-5">
		
			<nav class="text-center pb-5 large">
				<a href="" class="mr-2 border-bottom-left">SEMUA</a>
				<a href="" class="mr-2">AKTIVITAS</a>
				<a href="" class="mr-2">INSPIRASI</a>
			</nav>
			<div class="overflow-x">
				<div class="row gap ten text-center w-500 w-sm-400 w-md-200">
					<div>
						<img src="http://placeimg.com/320/480/any" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyb" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyc" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyd" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anye" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/any" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyb" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyc" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anyd" class="w-100"/>					
					</div>
					<div>
						<img src="http://placeimg.com/320/480/anye" class="w-100"/>					
					</div>
				</div>
			</div>			
		</div>
		
		
		
		<div class="carousel p-5" style={{height:"",backgroundImage:"url(/images/background.svg)"}}>
			<input id="slide-1" type="radio" name="carousel" checked/>
			<input id="slide-2" type="radio" name="carousel"/>
			<input id="slide-3" type="radio" name="carousel"/>
			<section class="p-5">
				<div class="row one four-md text-center center">
					<div class="md-3">
						<i class="icon-quote display-1 text-maroon"/>
						<p class="extra-large p-3">
							<i>"Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna."</i>
						</p>
						<h3 class="text-muted">Adam</h3>
						<img src="http://placeimg.com/640/360/any" class="avatar shadow extra-large"/>
					</div>
				</div>
				<div class="row one four-md text-center center">
					<div class="md-3">
						<i class="icon-quote display-1 text-maroon"/>
						<p class="extra-large p-3">
							<i>"Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna."</i>
						</p>						
						<h3 class="text-muted">Eve</h3>
						<img src="http://placeimg.com/640/360/anyb" class="avatar shadow extra-large"/>
					</div>
				</div>
				<div class="row one four-md text-center center">
					<div class="md-3">
						<i class="icon-quote display-1 text-maroon"/>
						<p class="extra-large p-3">
							<i>"Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna."</i>
						</p>						
						<h3 class="text-muted">Lord</h3>
						<img src="http://placeimg.com/640/360/anyc" class="avatar shadow extra-large"/>
					</div>
				</div>
			</section>
			<div class="indicators">
				<label for="slide-1">1</label>
				<label for="slide-2">2</label>
				<label for="slide-3">3</label>
			</div>
		</div>
		
		<div class="">
			<div class="p-5 red text-white text-center">
				<h1>Apakah Anda ingin bergabung?</h1>				
				<p>
					Lorem ipsum dolor sit amet, consetetur sadipscing esed diam nonumy eirmod tempor invidunt ut labore et dolore magna.
				</p>
				<br/>
				<button class="text-error white extra-large">Daftar Sekarang !!</button>
			</div>
		</div>
		
		
		<div class="p-5">
			<h2 class="text-center">Berita Terbaru</h2>
			
			<div class="row gap one three-sm p-5">
				<div>
					<div class="card">
						<img src="http://placeimg.com/240/480/any" class="w-100"/>		
					</div>
				</div>
				<div class="row gap one two-sm fill">
					<div class="sm-2">
						<div class="card">
							<img src="http://placeimg.com/480/220/any" class="w-100"/>		
						</div>
					</div>
					<div class="">
						<div class="card">
							<img src="http://placeimg.com/480/480/any" class="w-100"/>		
						</div>
					</div>
					<div class="">
						<div class="card">
							<img src="http://placeimg.com/480/480/any" class="w-100"/>		
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class='footer px-5 text-white'>
			<div class="row one three-md">
				<div class="py-5">
					<nav>
						<h3>Footer</h3>
						<ul>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
						</ul>
					</nav>
				</div>
				<div class="py-5">
					<nav>
						<h3>Footer</h3>
						<ul>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
						</ul>
					</nav>
				</div>
				<div class="py-5">
					<nav>
						<h3>Footer</h3>
						<ul>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
							<li><a class="text-white"><i class="icon-right-thin mr-1"/>Link</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="credit text-center p-2">
				Copyright &copy; 2020 <span class="font-gracea text-error">Dekranasda Jawa Timur</span>
			</div>
		</div>
	</div>
)
	
	
function isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    // Only completely visible elements return true:
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
}
