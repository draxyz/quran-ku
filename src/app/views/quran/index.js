import { h } from "hyperapp"
import { set,setHash, request } from "../../helpers/"
// import daftar from "../../../data/surat.json";
// import * as surat from "../../../data/surat/*.json";
// import { auth } from "../auth/"

const DaftarSurat=state=>(
	<div>	
		{/*<div class="px-4 px-md-5">
			<input placeholder="Surat"/>
			<br/>
		</div>*/}
		<div class="px-4 px-md-5">
			<h3>Daftar Surat</h3>
		</div>
		{state.quran.data.daftar.map(d=>(
			<div class={`px-4 px-md-5 py-2 ${d.nomor%2&&'even'}`}>
				<div class="tile w-100 cursor-pointer border-bottom m-0" onclick={[actions.getSurat,d.nomor]}>
					<div style={{width:'3.5em'}}>
						<h3 class="p-1 m-0 text-right">{d.nomor}</h3>
					</div>
					<div class="py-1">
						<h5 class="m-0">{d.nama}</h5>
						<p class="m-0 text-secondary">{d.arti}</p>
					</div>
					<div>
						<h2 class="p-1 font-utsmani text-right line-height-1">{d.asma}</h2>
					</div>
				</div>
			</div>
		))}
		<div style={{height:'15em'}}/>
	</div>
)
const Header=state=>(
		<div class="tile px-4 px-md-5 py-2 m-0 sticky top-left white shadow" style={{zIndex:1001}}>
			<a href="#!/" class="button silver text-black py-1 px-2"><i class="icon-left-open"/> Kembali</a>
		</div>
)


// <div class="dropdown">
				// <a class="button silver text-black" data-dropdown tabindex="0">Quran &#x25BE;</a>
				// <ul class="menu">
					// <li><a href="#!/">Beranda</a></li>
					// <li><a href="#!/file">File</a></li>
					// <li><a href="#!/users">Users</a></li>
				// </ul>
			// </div>		
const AudioSurat=({app,quran})=>(
	<div class="fixed bottom-left w-100">	
		<input type="checkbox" id="audio-surat" checked/>
		<article class="toast silver border-0 m-0 p-4">
			<audio controls src={quran.data.daftar[app.current-1].audio.replace('http://','https://')} type="audio/mpeg">
				Your device does not support the audio.
			</audio>
		</article>
	</div>
)
export const QuranSurat=({app,quran})=>(
	<div class="relative">
		{AudioSurat({app,quran})}
		<div class="tile px-4 px-md-5 py-2 m-0 sticky top-left white shadow" style={{zIndex:1001}}>
			<div>
				<a href="#!/quran" class="p-1 button silver text-black"><i class="icon-menu"/> Daftar Surat &nbsp;</a>
			</div>	
			<div class="text-center p-2">{quran.message.text}</div>
			<div class="action">
				<label for="audio-surat" class="button link mr-2 p-1"><i class="icon-sound"/></label>
				
				<div class="group py-1">	
					<button class="p-1 tooltip-left" onclick={[actions.getSurat,app.current-1]} data-tooltip={(quran.data.daftar[app.current-2]||{}).nama} disabled={app.current=='1'}>
						<i class="icon-left-open"/>
					</button>					
					<button class="p-1 tooltip-left" onclick={[actions.getSurat,parseInt(app.current)+1]} data-tooltip={(quran.data.daftar[app.current]||{}).nama} disabled={app.current=='114'}>
						<i class="icon-right-open"/>
					</button>
				</div>
			</div>
		</div>
		
		<div class="px-4 px-md-5 py-2">
			<div class="tile m-0">
				<h3>
					{app.current}
					. &nbsp;
					{quran.data.daftar[app.current-1].nama}
				</h3>
				<div/>
				<label for="detail-surat" class="button info action"><i class="icon-info"/></label>
			</div>
			<div class="tile m-0">
				<div class="text-primary">
					<b>
					Arti : &nbsp;
					{quran.data.daftar[app.current-1].arti}
					</b>
				</div>
				<div/>
				<div class="action">
					{quran.data.daftar[app.current-1].ayat}
					&nbsp; ayat
				</div>
			</div>
			<div class=" border-bottom">
				<input type="checkbox" id="detail-surat" checked/>
				<article class="card border-0">
					<html-insert html={quran.data.daftar[app.current-1].keterangan}/>
				</article>
			</div>
		</div>
		{quran.data.surat[app.current].map(s=>(
			<div class={`px-4 px-md-5 py-2 ${s.nomor%2&&'even'}`}>
				<div id={`ayat-${s.nomor}`} class='tile text-black w-100 m-0 border-bottom' >
					<div>
						<div class="sticky border rounded px-2" style={{top:'5em'}}>
						{s.nomor}
						</div>
					</div>
					<div class="text-right">
						<h2 class="font-utsmani">
							<html-insert html={s.ar.replace('بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ','ِبِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ<br>')}/>
						</h2>
						<p class="m-0 text-info"><html-insert html={s.tr}/></p>
						<p class="m-0">{s.id}</p>
					</div>
				</div>
			</div>
		))}
		<div style={{height:'15em'}}/>
	</div>
)
export const Quran=state=>(
	<div cass="relative">
		<Header/>
		{DaftarSurat(state)}
	</div>
)



/* Initial state */
export const initQuran={
	message:{text:''},
	data:{
		daftar:[],
		surat:[],
		cache:true
	}
}


const actions={
	getDaftar:(state,ev)=>[
		state,
		ev.preventDefault(),
		state.quran.data.daftar!=[]?
			request.get({
				url:`./data/surat.json`,
				action:(state,response)=>[
					state,
					set('quran.data.daftar',response),
					setHash('#!/quran')
				],
				error:(state,response)=>[
					state,
					console.log(response)
				]
			})
			:
				setHash('#!/quran')
			
	],
	
	getSurat:(state,id)=>[
		state,
		// console.log(state.quran.data.surat[id]),
		set('quran.message.text','memuat'),
		state.quran.data.surat[id]==null?
			request.get({
				url:`./data/surat/${id}.json`,
				action:(state,response)=>[
					state,
					set(`quran.data.surat.${id}`,response),
					set('app.current',id),
					set('quran.message.text',''),
					setHash(`#!/quran/surat/${id}`)
				],
				error:(state,response)=>[
					state,
					console.log(response)
				]
			})
			:[
				set('quran.message.text','',500),
				set('app.current',id,500),
				setHash(`#!/quran/surat/${id}`)
			]
			
	],
	
}

export const actionQuran=actions;


function scrollToItem(item) {
    var diff=(item.offsetTop-window.scrollY)/20;
    if(!window._lastDiff){
        window._lastDiff = 0;
    }

    // console.log('test')

    if (Math.abs(diff)>2) {
        window.scrollTo(0, (window.scrollY+diff))
        clearTimeout(window._TO)

        if(diff !== window._lastDiff){
            window._lastDiff = diff;
            window._TO=setTimeout(scrollToItem, 15, item);
        }
    } else {
        console.timeEnd('test');
        window.scrollTo(0, item.offsetTop)
    }
}