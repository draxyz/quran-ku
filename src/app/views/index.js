import { h } from "hyperapp"
import { set } from "../helpers/"
import { lang } from "../lang/"
import { Login, auth } from "./auth/"
import { Home } from "./home/"
import { Quran, QuranSurat } from "./quran/"
import { Users, users } from "./users/"
import { Error } from "./error/"



const ROUTE=[
  {path:'#!/home',render:Home},
  {path:'#!/quran/surat',render:QuranSurat},
  {path:'#!/quran',render:Quran},
  {path:'#!/users',render:Users},
  {path:'404',render:Home},
]
const pageActive=(path,classname)=>(
	window.location.hash.indexOf(path)==0?classname:''
)

const buildTree =(node)=>(
	<ul class="tree">
		{typeof node=="object"&&Object.keys(node).map(s=>
			<li>
				{(typeof node[s]!="string"&&node[s]!=undefined)&&Object.keys(node[s]).length>0?
					<span>
						<input type="checkbox" id={"tree-"+JSON.stringify(node[s])+s}/>
						<label for={"tree-"+JSON.stringify(node[s])+s}>{s}</label>
						{buildTree(node[s])}
					</span>
					:
					<span>
					{s} : {JSON.stringify(node[s])}
					</span>
				}
			</li>
		)}
	</ul>
)



export const view=state=>(
	<div>
	{state.app.loading?
		<div>Loading</div>
		:
		
		<div>
			{ROUTE.find(r=>(
				r.path==state.app.page
					|| !r.exact&&(state.app.page).indexOf(r.path)==0
						|| r.path=='404'
			)).render(state)}
			
			
			{/*<div class="collapse">
				<input type="checkbox" id="current-state"/>
				<label for="current-state">View State</label>
				<nav class="small">{buildTree(state)}</nav>
			</div>*/}
		</div>
	}
	
	</div>
)
	