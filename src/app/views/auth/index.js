import { h, app } from "hyperapp"
import { set, setHash, merge, request } from "../../helpers/"
import { lang } from "../../lang/"


/* Views */
export const Login=({state})=>(
	<section class="row one four-sm center middle" style={{height:"100vh"}}>
		<div class="white shadow text-primary border-bottom-right rounded sm-3 p-5">
			<div class="row one two-md">
				<div class="flex center middle">
					<img alt="" src="/images/avatar.png"/>
				</div>
				<form class="text-black py-5 px-3" onsubmit={auth.loginSubmit}>
					<h1 class="text-center p-0">Welcome</h1>
					<h4 class="text-center text-muted p-0">Login to continue</h4>
					<p class="text-center text-muted">
						<html-insert html={state.auth.message.status||'&nbsp;'}/>
					</p>
					<label for="username">Username</label>
					<input id="username" name="username" type="text" placeholder="Username"/>
					<p class="text-error m-0">
						{state.auth.message.username} &nbsp;
					</p>
					<label for="password">Username</label>
					<input id="password" name="password" type="password" placeholder="Password"/>
					<p class="text-error m-0">
						{state.auth.message.password} &nbsp;
					</p>
					<button type="submit" class="secondary w-100 my-2">Login</button>
				</form>
			</div>
			<div class="collapse">
			
				<input type="checkbox" id="current-state"/>
				<label for="current-state">View State</label>
				<pre class="small">{JSON.stringify(state,null,2)}</pre>
			</div>
		</div>
	</section>
	
)

/* Initial state */
export const initAuth={
	message:{
		status:''
	},
	logged:{
		id:false,
		username:'',
		avatar:'',
		token:false,
		cache:true
	}
}

/* actions */
export const auth={
	loginSubmit:(state,ev)=>[
		state,
		ev.preventDefault(),
		request.post({
			url:`${state.app.url}/tests/?mode=auth`,
			options:{body:new FormData(ev.target)},
			action:auth.loginSuccess,
			error:auth.loginError
		}),
		set('auth.message',{status:lang('auth.submit')}),
	],
	
	loginSuccess:(state,response)=>[
		state,
		response.id&&[
			set('auth.message',{status:lang('auth.success')},500),
			set('auth.logged',merge(initAuth.logged,response),3000),
			setHash('#!/dashboard')
		],
		set('auth.message',{},3000)
	],
	
	loginError:(state,response)=>[
		state,
		set('auth.message',{
			status:lang('auth.error',response.message),
			username:response.message
		},500)
	],
		
	logoutSubmit:(state)=>[
		state,
		set('auth',initAuth)
	]
}
