import { h } from "hyperapp"
import { set , request , setHash , run  } from "../../helpers/"
import { auth } from "../auth/"

/* Views */
export const Users=({users:{status,list},users})=>(
	<div>
		{/* Action bar */}
		<div class="tile">		
			<div class="dropdown">
				<a class="button silver text-black pr-1" data-dropdown tabindex="0">
					Users
					<i class="icon-down-open ml-2"/>
				</a>
				<ul class="menu">
					<li><a href="#!/dashboard">Dashboard</a></li>
					<li><a href="#!/tasks">Tasks</a></li>
					<li><a href="#!/file">File</a></li>
				</ul>
			</div>
			<div/>
			<div class="action">
				<button class="link transparent px-1" aria-label="User Refresh" onclick={action.refresh}>
					<i class={`icon-arrows-ccw ${status.loading&&"icon-spin"}`}/>
					<span class={!status.loading&&"none"}>Loading</span>
				</button>
				<label class="button link transparent px-1" aria-label="User Toolbar" for="user-toolbar">
					<i class="icon-search"/>
				</label>
				<div class="dropdown dropdown-right">
					<button class="link transparent px-1" aria-label="Open Menu" data-dropdown tabindex="0">
						<i class="icon-menu"/>
					</button>
					<ul class="menu">
						<li><a href="#!/users" onclick={auth.logoutSubmit}>Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		{/* View actions */}
		<div class="">			
			<div class="collapse border-0">
				<input type="checkbox" id="user-toolbar"/>
				<label for="user-toolbar" class="none"/>
				<section>
					<div class="group w-100">
						<input name="search" placeholder="Search" aria-label="User Search"/>
							{/*<button class="muted"><i class="icon-search"/></button>*/}
					</div>
					
					
					
					<div class="row three">
						<div class="fill">
							<span class="nowrap">
								Sort : 
								<span class="dropdown ml-1 mr-3">
									<a class="text-info hand" data-dropdown tabindex="0">{list.option.sort}</a>
									<ul class="menu">
										{list.config.sorts.map(sort=>						
											<li><a onclick={[action.filter,{"option.sort":sort}]}>{sort}</a></li>
										)}
									</ul>
								</span>
							</span>
							<span class="nowrap">
								Order : 
								<span class="dropdown ml-1 mr-3">
									<a class="text-info hand" data-dropdown tabindex="0">{list.option.order}</a>
									<ul class="menu">
										{list.config.orders.map(order=>						
											<li><a onclick={[action.filter,{"option.order":order}]}>{order}</a></li>
										)}
									</ul>
								</span>				
							</span>
						</div>
					
						<div class="text-right">
							<span class="">Page : </span>
							<span class="nowrap">
								<span class="dropdown dropdown-right text-left">
									<a class="text-info hand" data-dropdown tabindex="0">{parseInt(list.option.offset/list.option.limit)+1}</a>
									<ul class="menu">
										{Array.apply(null,Array(Math.ceil(list.config.total/list.option.limit))).map((x,offset)=>
											<li>
												<a onclick={[action.filter,{"option.offset":offset*list.option.limit}]}>{offset+1}</a>
											</li>
										)}
									</ul>
								</span>
								<span class="px-1">of</span>
								<span class="text-info">
									{Math.round(list.config.total/list.option.limit)} 
								</span>
							</span>
						</div>
					</div>
					
					
					
				</section>
			</div>
				
			
			
			<input type="checkbox" id="users-add" checked/>
			<article class="card py-2">
				<header>Add User</header>
				<label for="users-add" class="close">&times;</label>
				<section>
					<form onsubmit={action.add}>
						<label>
							Username
							<input name="username" placeholder="Username"/>
						</label>
						<label>
							Groupname
							<input name="groupname" placeholder="Groupname"/>
						</label>
						<button>Save</button>
					</form>
				</section>
			</article>
			
			{/* View Data */}
			<section class="py-3">
				{list.data.map(user=>
					<article class="tile border-bottom mt-0 py-1">
						<img alt="" src="/images/avatar.png" class="avatar small mr-2"/>
						<section class="py-1">
							<div class="tile m-0">
								<h5 class="text-ellipsis" style={{width:'calc(100% - 3em)'}}>
									{user.username}
								</h5>
								<div class="dropdown dropdown-right action">
									<a class="small" data-dropdown tabindex="0">
										<i class="icon-dot-3"/>
									</a>
									<ul class="menu">
										<li><a href="#!/users/edit"><i class="icon-pencil"/> Edit</a></li>
										<li><a href="#!/users/remove"><i class="icon-cancel"/> Remove</a></li>
									</ul>
								</div>
							</div>
							<span class="text-gray">{user.groupname}</span>
						</section>
					</article>
				)}
			</section>
			<label class="button fab" for="users-add">
				<i class="icon-plus"/>
			</label>
		</div>
		
	</div>
)


/* Initial state */
export const initUsers={
	status:{
		mode:'',
		loading:false,
	},
	form:{
		
	},
	list:{
		config:{
			sorts:['id','username','groupname'],
			orders:['asc','desc'],
			total:1,
		},
		option:{
			search:'',
			sort:'id',
			order:'desc',
			limit:5,
			offset:0			
		},
		data:[{
			"id":false,
			"username":"",
			"groupname":"",
			"avatar":""
		}],
		cache:true
	}
}

const action={
	refresh:(state,ev)=>[
		state,
		setHash('#!/users'),
		set('users.status.loading',true),
		// console.log(new URLSearchParams(list.option).toString()),
		request.get({
			url:`${state.app.url}/tests/?mode=users&`+new URLSearchParams(state.users.list.option).toString(),
			options:{
				headers:{'Authorization':state.auth.logged.token}
			},
			action:(state,response)=>[
				state,
				set('users.list.config.total',response.total),
				set('users.list.data',response.rows),
				set('users.status.loading',false,2000),
				// console.log(response.rows)
			]
		}),
	],
	
	
	filter:(state,props)=>[
		state,
		props&&Object.keys(props).map(key=>
			set(`users.list.${key}`,props[key])
		),
		run(users.refresh)
	],
	
	add:(state,ev)=>[
		state,
		ev.preventDefault(),
		request.post({
			url:`${state.app.url}/tests/?mode=users`,
			options:{
				headers:{'Authorization':state.auth.logged.token},
				body:new FormData(ev.target)
			},
			action:(state,response)=>[
				state,
				// set('users.list.data',response.rows),
				set('users.status.loading',false,2000),
				run(users.refresh)
			]
		}),
		set('users.status.loading',true),
	],
	

}
export const users=action;