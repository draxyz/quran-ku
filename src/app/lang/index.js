import en from "./eng.js"

const i18n={
	en
}
export const lang=(key,str)=>{
	let language=document.documentElement.lang,
			result=(key.split('.').reduce((p,c)=>p&&p[c]||null,i18n[language]))
  return result.replace('%s',str||'%s')
}