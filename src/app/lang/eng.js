import { h } from "hyperapp"

export default{
	auth:{
		submit:"Authenticating",
		success:"<span class='text-info'>You're Logged In, Redirecting</span>",
		error:"<span class='text-error'>Error (%s)</span>"
	}
}
